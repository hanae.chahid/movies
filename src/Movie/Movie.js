import React, { Component } from 'react'
import { API_KEY, API_URL} from '../config'
import Navigation from '../components/Navigation/Navigation'
import MovieInfoBar from '../components/MovieInfoBar/MovieInfoBar'
import MovieInfo from '../components/MovieInfo/MovieInfo'
import FourColGrid from '../components/FourColGrid/FourColGrid'
import Spinner from '../components/Spinner/Spinner'
import Actor from '../components/Actor/Actor'


class Movie extends Component {
    state={
        movie: null,
        actors: null,
        directors: [],
        loading: false
    }

    componentDidMount() {
        this.setState({ loading: true});
        const id=this.props.match.params.movieId;
        //first fetch the movie...
        const endpoint= `${API_URL}movie/${id}?api_key=${API_KEY}&language=en-US`;
        this.fetchItems(endpoint);
    }

    fetchItems = ( endpoint) => {
        fetch(endpoint)
        .then(result => result.json())
        .then( result => {
            console.log(this.props)
            if( result.status_code){
                this.setState({ loading: false}); 
            }else{
                this.setState({ movie: result}, () => {
                    // then fetch actors
                    const endpoint= `${API_URL}movie/${this.props.match.params.movieId}credits?api_key=${API_KEY}`;
                    fetch(endpoint)
                    .then( res => res.json())
                    .then(res => {
                        const directors= res.crew.filter( (member) =>  member.job === "Director") ;
                        this.setState({
                            actors: result.cast,
                            directors,
                            loading: false
                        })
                    })
                })
            }
        })
        .catch(err => console.log('Error', err))
    }
    render(){
        return(
            <div className="movie">
                <Navigation movieName="hola movie" />
                <MovieInfo />
                <MovieInfoBar />
                <Spinner />
            </div>
        )
    }
}



export default Movie