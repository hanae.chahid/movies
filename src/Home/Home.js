import React, { Component } from 'react';
import HeroImage from '../components/HeroImage/HeroImage'
import SearchBar from '../components/SearchBar/SearchBar'
import FourColGrid from '../components/FourColGrid/FourColGrid'
import MovieThumb from '../components/MovieThumb/MovieThumb'
import LoadMoreBtn from '../components/LoadMoreBtn/LoadMoreBtn'
import Spinner from '../components/Spinner/Spinner'
import './Home.css';
//import Config.js 
import { API_URL, API_KEY, IMAGE_BASE_URL, BACKDROP_SIZE, POSTER_SIZE} from '../config'

class Home extends Component {

    constructor(props){
        super(props);
        this.state= {
            movies: [],
            heroImage: null,
            loading: false,
            currentPage: 0,
            totalPages: 0,
            searchItem: ''
        }
    }

    componentDidMount(){
        this.setState({
            loading: false
        });
        const endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
        this.fetchItems(endpoint);
    }

    searchItems= (searchTerm) => {
        console.log(searchTerm);
        let endpoint = "";
        this.setState({
            movies: [],
            loading: true,
            searchItem: ''
        })

        if(searchTerm === ''){
            endpoint= `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=1`;
        }else{
            endpoint= `${API_URL}movie/popular?api_key=${API_KEY}&query=${searchTerm}&language=en-US`;
        }
        this.fetchItems(endpoint);
    }
    
    loadMoreItems = () => {
        let endpoint='';
        this.setState({ loading: false});
        if(this.state.searchItem === '') {
            endpoint= `${API_URL}movie/popular?api_key=${API_KEY}&language=en-US&page=${ this.state.currentPage + 1}`;
        }
        else{
            endpoint=`${API_URL}movie/search/movies?api_key=${API_KEY}&language=en-US&query=${this.state.searchItem}&page=${ this.state.currentPage + 1}`;
        }
        this.fetchItems(endpoint);
    }

    fetchItems = ( endpoint ) => {
        fetch(endpoint)
        .then( result => result.json())
        .then( resJson => {
            this.setState({
                movies: [...this.state.movies,...resJson.results],
                heroImage: this.state.heroImage ? this.state.heroImage: resJson.results[0],
                loading: false,
                currentPage: resJson.page,
                totalPages: resJson.total_results
            });
        })
    }

    render() {
        return (
        <div className="home">
            {this.state.heroImage ? (
                <div>
                <HeroImage 
                    title={this.state.heroImage.original_title}
                    text={this.state.heroImage.overview}
                    image={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${this.state.heroImage.backdrop_path}`}
                />
                <SearchBar searchItems={this.searchItems}/>
            </div>
            ) : null
            }
            <div className="home-grid">
                 <FourColGrid 
                    header= { this.state.searchItem ? 'Search result': 'popular movies'}
                    loading={this.state.loading}
                 >
                 {
                     this.state.movies.map((movie, i) => {
                         return (
                            <MovieThumb key={i}
                                clickable={true}
                                image= { movie.poster_path ? `${IMAGE_BASE_URL}${POSTER_SIZE}${movie.poster_path}` : './images/no_image.jpg'}
                                movieId={movie.id}
                                movieName= {movie.original_title}
                            />
                         )
                     })
                 }
                 </FourColGrid>
                { this.state.loading ? <Spinner /> : null }
                {(this.state.currentPage <= this.state.totalPages && !this.state.loading) ? 
                        <LoadMoreBtn text="Load more" onClick={this.loadMoreItems} />
                        : null
                }
            </div>
          
        </div>
        );
    }
}

export default Home;