import React, { Component } from 'react';
import { BrowserRouter, Switch, Route} from 'react-router-dom'
import Header from '../components/Header/Header';
import Home from '../Home/Home';
import Movie from '../Movie/Movie'
import NotFound from '../components/NotFound/NotFound'

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div >
          <Header />
          <Switch >
            <Route exact path="/" component={Home} />
            <Route path="/movie/:movieId" component={Movie} />
            <Route component={NotFound} />
          </Switch>
          
        </div>
      </BrowserRouter>
      
    );
  }
}

export default App;
