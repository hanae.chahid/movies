import React, { Component} from 'react'
import FonAwsome from 'react-fontawesome'
import './SearchBar.css'

class SearchBar extends Component {
    state = {
        value: ''
    }

    timeout= null;

    doSearch = (event) => {
        this.setState({
            value: event.target.value
        })
        clearTimeout(this.timeout);
        this.timeout = setTimeout( () => {
            this.props.searchItems(this.state.value);
        }, 500)
    }

    render(){
    return(
        <div className="searchbar">
            <div className="searchbar-content">
                <FonAwsome className="searchbar-icon" name="search" size="2x" />
                <input type="text" className="searchbar-input" placeholder="search"
                    onChange={this.doSearch} value={this.state.value}
                />
            </div>
        </div>
    )
    }
}

export default SearchBar;