import React from 'react'
import { Link } from 'react-router-dom'
import './MovieThumb.css'

const MovieThumb = (props) => {
    return(
        <div className="movie-thumb">
            { props.clickable ? (
                <Link to={{ pathname: `/movie/${props.movieId}`, movieName: `${props.movieName}`}}>
                    <img src={props.image} alt="movieThumb" className="clickable"/>
                </Link>
            ) : 
                <img src={props.image} alt="movieThumb" className="clickable"/>
            }
        </div>
    )
    
}

export default MovieThumb;