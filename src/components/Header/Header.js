import React from 'react';
import { Link } from 'react-router-dom'
import './Header.css';

const Header = () => {

        return (
        <div className="header" >
            <div className="header-content">
                <Link to="/">
                    <img src="./images/reactMovie_logo.png" alt="" className="logo"/>
                </Link>
                <img src="./images/tmdb_logo.png" alt="" className="tmdb-logo"/>
            </div>
        </div>
        )
    
}

export default Header;