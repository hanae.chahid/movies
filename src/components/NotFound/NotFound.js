import React from 'react'


const NotFound = () => {
    return(
        <div>
            <h3>Ouups, this movie doesn't exist!</h3>
        </div>
    )
}

export default NotFound